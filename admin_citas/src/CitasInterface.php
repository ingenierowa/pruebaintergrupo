<?php
namespace Drupal\admin_citas;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Citas entity.
 * @ingroup admin_citas
 */
interface CitasInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
?>