<?php

namespace Drupal\admin_citas\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Pruebas basicas para la entidad content_entity_admin_citas
 */
class CitasEntityTest extends WebTestBase {

  public static $modules = ['admin_citas'];

  /**
   * Prueba acceso denegado usuarios anonimos pagina de reporte de citas
   */

  public function prueba_acceso_denegado() {
    $this->drupalGet('content_entity_admin_citas_reporte');
    $this->assertResponse(200, 'You are not authorized to access this page.');
  }

  /**
   * Prueba las rutas expuestas por el modulo con sus permisos
   */
  public function prueba_rutas() {
    $assert = $this->assertSession();
    $entity_storage = \Drupal::entityTypeManager()
      ->getStorage('content_entity_admin_citas');
    $new_appointment = $entity_storage->create([
      'documento' => '86101364',
      'nombre' => 'fredy aristizabal',
      'mail' => 'fred@hotmail.com',
      'fecha' => '1500925328',
      'descripcion' => 'Cita de revision',
    ]);
    $new_appointment->save();
    $data = $this->probar_rutas($new_appointment->id());
    // Ejecutar las pruebas.
    foreach ($data as $datum) {
      if ($datum[2]) {
        $user = $this->drupalCreateUser([$datum[2]]);
        $this->drupalLogin($user);
      }
      else {
        $user = $this->drupalCreateUser();
        $this->drupalLogin($user);
      }
      $this->drupalGet($datum[1]);
      $assert->statusCodeEquals($datum[0]);
    }
  }

  /**
   * Funcion para probar las rutas
   *
   * @param int $new_appointment
   *   El id de la entidad de citas
   *
   * @return array
   *   Matriz de datos de prueba ordenados por:
   *   - Código de respuesta esperada.
   *   - Ruta de la peticion.
   *   - Permisos del usuario.
   */
  protected function probar_rutas($new_appointment) {
    return [
      [
        200,
        '/content_entity_admin_citas/' . $new_appointment,
        'view citas entity',
      ],
      [
        403,
        '/content_entity_admin_citas/' . $new_appointment,
        '',
      ],
      [
        200,
        '/content_entity_admin_citas_reporte',
        'view citas entity',
      ],
      [
        200,
        '/content_entity_admin_citas/add',
        'add citas entity',
      ],
      [
        403,
        '/content_entity_admin_citas/add',
        '',
      ],
      [
        200,
        '/content_entity_admin_citas/' . $new_appointment . '/edit',
        'edit citas entity',
      ],
      [
        403,
        '/content_entity_admin_citas/' . $new_appointment . '/edit',
        '',
      ],
    ];
  }

  /**
   * Prueba agregar nuevos campos a la entidad de citas.
   */
	public function prueba_agregar_campos() {
    $web_user = $this->drupalCreateUser(array(
      'administer citas entity',
    ));
    $this->drupalLogin($web_user);
    $entity_name = 'content_entity_admin_citas';
    $add_field_url = 'admin/structure/' . $entity_name . '_settings/fields/add-field';
    $this->drupalGet($add_field_url);
    $field_name = 'test_name';
    $edit = array(
      'new_storage_type' => 'list_string',
      'label' => 'test name',
      'field_name' => $field_name,
    );
    $this->drupalPostForm(NULL, $edit, t('Save and continue'));
    $expected_path = $this->buildUrl('admin/structure/' . $entity_name . '_settings/fields/' . $entity_name . '.' . $entity_name . '.field_' . $field_name . '/storage');
    // Buscar url sin parámetros de consulta.
    $current_path = strtok($this->getUrl(), '?');
    $this->assertEquals($expected_path, $current_path);
  }
}
