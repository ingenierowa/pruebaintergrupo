<?php


namespace Drupal\admin_citas\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Language\Language;
use Drupal\Core\Form\FormStateInterface;

/**
 * Controlador de formularios para editar y agregar campos de la entidad
 * content_entity_admin_citas.
 *
 * @ingroup admin_citas
 */
class CitasForm extends ContentEntityForm {

	/**
	 * {@inheritdoc}
	 */
	public function buildForm(array $form, FormStateInterface $form_state) {
		/* @var $entity \Drupal\admin_citas\Entity\Citas */
		$form = parent::buildForm($form, $form_state);
		$entity = $this->entity;
		return $form;
	}

	/**
	 * Funcion que guarda la información de la entidad y al momento de guardar
	 * envia al usuario al reporte de citas
	 */
	public function save(array $form, FormStateInterface $form_state) {
		$form_state->setRedirect('admin_citas.formreporte');
		$entity = $this->getEntity();
		$entity->save();
	}
}

?>