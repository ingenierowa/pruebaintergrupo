<?php
/**
 * @file
 * Contains \Drupal\admin_citas\Entity\Contact.
 */

namespace Drupal\admin_citas\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\admin_citas\CitasInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Contact entity.
 *
 * @ingroup admin_citas
 *
 * This is the main definition of the entity type. From it, an entityType is
 * derived. The most important properties in this example are listed below.
 *
 * id: The unique identifier of this entityType. It follows the pattern
 * 'moduleName_xyz' to avoid naming conflicts.
 *
 * label: Human readable name of the entity type.
 *
 * handlers: Handler classes are used for different tasks. You can use
 * standard handlers provided by D8 or build your own, most probably derived
 * from the standard class. In detail:
 *
 * - view_builder: we use the standard controller to view an instance. It is
 *   called when a route lists an '_entity_view' default for the entityType
 *   (see routing.yml for details. The view can be manipulated by using the
 *   standard drupal tools in the settings.
 *
 * - list_builder: We derive our own list builder class from the
 *   entityListBuilder to control the presentation.
 *   If there is a view available for this entity from the views module, it
 *   overrides the list builder. @todo: any view? naming convention?
 *
 * - form: We derive our own forms to add functionality like additional fields,
 *   redirects etc. These forms are called when the routing list an
 *   '_entity_form' default for the entityType. Depending on the suffix
 *   (.add/.edit/.delete) in the route, the correct form is called.
 *
 * - access: Our own accessController where we determine access rights based on
 *   permissions.
 *
 * More properties:
 *
 *  - base_table: Define the name of the table used to store the data. Make
 *   sure
 *    it is unique. The schema is automatically determined from the
 *    BaseFieldDefinitions below. The table is automatically created during
 *    installation.
 *
 *  - fieldable: Can additional fields be added to the entity via the GUI?
 *    Analog to content types.
 *
 *  - entity_keys: How to access the fields. Analog to 'nid' or 'uid'.
 *
 *  - links: Provide links to do standard tasks. The 'edit-form' and
 *    'delete-form' links are added to the list built by the
 *    entityListController. They will show up as action buttons in an
 *   additional
 *    column.
 *
 * There are many more properties to be used in an entity type definition. For
 * a complete overview, please refer to the '\Drupal\Core\Entity\EntityType'
 * class definition.
 *
 * The following construct is the actual definition of the entity type which
 * is read and cached. Don't forget to clear cache after changes.
 *
 * @ContentEntityType(
 *   id = "content_entity_admin_citas",
 *   label = @Translation("Citas entity"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" =
 *   "Drupal\admin_citas\Entity\Controller\CitasListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\admin_citas\Form\CitasForm",
 *       "edit" = "Drupal\admin_citas\Form\CitasForm",
 *       "delete" = "Drupal\admin_citas\Form\CitasDeleteForm",
 *     },
 *     "access" = "Drupal\admin_citas\CitasAccessControlHandler",
 *   },
 *   base_table = "citas",
 *   admin_permission = "administer admin_citas entity",
 *   fieldable = TRUE,
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" =
 *   "/content_entity_admin_citas/{content_entity_admin_citas}",
 *     "edit-form" =
 *   "/content_entity_admin_citas/{content_entity_admin_citas}/edit",
 *     "delete-form" = "/citas/{content_entity_admin_citas}/delete",
 *     "collection" = "/content_entity_admin_citas/list"
 *   },
 *   field_ui_base_route = "admin_citas.citas_settings",
 * )
 *
 * The 'links' above are defined by their path. For core to find the
 *   corresponding route, the route name must follow the correct pattern:
 *
 * entity.<entity-name>.<link-name> (replace dashes with underscores)
 * Example: 'entity.content_entity_admin_citas.canonical'
 *
 * See routing file above for the corresponding implementation
 *
 * The 'Contact' class defines methods and fields for the contact entity.
 *
 * Being derived from the ContentEntityBase class, we can override the methods
 * we want. In our case we want to provide access to the standard fields about
 * creation and changed time stamps.
 *
 * Our interface (see CitasInterface) also exposes the EntityOwnerInterface.
 * This allows us to provide methods for setting and providing ownership
 * information.
 *
 * The most important part is the definitions of the field properties for this
 * entity type. These are of the same type as fields added through the GUI, but
 * they can by changed in code. In the definition we can define if the user
 *   with
 * the rights privileges can influence the presentation (view, edit) of each
 * field.
 */
class Citas extends ContentEntityBase implements CitasInterface {

	/**
	 * {@inheritdoc}
	 *
	 * Cuando se agrega una nueva instancia de entidad, se hace la referencia de
	 * entidad user_id usuario actual como creador de la instancia.
	 */
	public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
		parent::preCreate($storage_controller, $values);
		$values += [
			'user_id' => \Drupal::currentUser()->id(),
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function getCreatedTime() {
		return $this->get('created')->value;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getChangedTime() {
		return $this->get('changed')->value;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setChangedTime($timestamp) {
		$this->set('changed', $timestamp);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getChangedTimeAcrossTranslations() {
		$changed = $this->getUntranslated()->getChangedTime();
		foreach ($this->getTranslationLanguages(FALSE) as $language) {
			$translation_changed = $this->getTranslation($language->getId())
				->getChangedTime();
			$changed = max($translation_changed, $changed);
		}
		return $changed;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getOwner() {
		return $this->get('user_id')->entity;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getOwnerId() {
		return $this->get('user_id')->target_id;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setOwnerId($uid) {
		$this->set('user_id', $uid);
		return $this;
	}

	/**
	 * {@inheritdoc}
	 */
	public function setOwner(UserInterface $account) {
		$this->set('user_id', $account->id());
		return $this;
	}

	/**
	 * {@inheritdoc}
	 *
	 * Propiedades de los campos.
	 *
	 * Nombre del campo, el tipo
	 *
	 */

	public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
		$fields['id'] = BaseFieldDefinition::create('integer')
			->setLabel(t('ID'))
			->setDescription('ID de la entidad')
			->setReadOnly(TRUE);

		$fields['uuid'] = BaseFieldDefinition::create('uuid')
			->setLabel(t('UUID'))
			->setDescription('UUID creador de la entidad.')
			->setReadOnly(TRUE);

		$fields['documento'] = BaseFieldDefinition::create('integer')
			->setLabel('Documento de identificación')
			->setRequired(TRUE)
			->setSettings([
				'default_value' => '',
				'max_length' => 10,
			])
			->setDisplayOptions('view', [
				'label' => 'above',
				'type' => 'integer',
				'weight' => -6,
			])
			->setDisplayOptions('form', [
				'type' => 'integer',
				'weight' => -6,
			])
			->setDisplayConfigurable('form', TRUE)
			->setDisplayConfigurable('view', TRUE);

		$fields['nombre'] = BaseFieldDefinition::create('string')
			->setLabel(('Nombre completo'))
			->setRequired(TRUE)
			->setSettings([
				'max_length' => 255,
				'text_processing' => 0,
			])
			->setDisplayOptions('view', [
				'label' => 'above',
				'type' => 'string',
				'weight' => -5,
			])
			->setDisplayOptions('form', [
				'type' => 'string',
				'weight' => -5,
			])
			->setDisplayConfigurable('form', TRUE)
			->setDisplayConfigurable('view', TRUE);

		$fields['mail'] = BaseFieldDefinition::create('email')
			->setLabel(t('Email'))
			->setRequired(TRUE)
			->setDefaultValue('')
			->setSettings([
				'default_value' => '',
				'max_length' => 255,
				'text_processing' => 0,
			])
			->setDisplayOptions('view', [
				'label' => 'above',
				'type' => 'string',
				'weight' => -5,
			])
			->setDisplayOptions('form', [
				'type' => 'string',
				'weight' => -5,
			])
			->setDisplayConfigurable('form', TRUE)
			->setDisplayConfigurable('view', TRUE);

		$fields['fecha'] = BaseFieldDefinition::create('created')
			->setLabel(('Fecha y hora'))
			->setRequired(TRUE)
			->setDisplayOptions('view', [
				'label' => 'above',
				'type' => 'timestamp',
				'weight' => -4,
			])
			->setDisplayOptions('form', [
				'type' => 'datetime_timestamp',
				'weight' => -4,
			])
			->setDisplayConfigurable('form', TRUE)
			->setDisplayConfigurable('view', TRUE);

		$fields['descripcion'] = BaseFieldDefinition::create('text_long')
			->setLabel(('Descripción'))
			->setRequired(TRUE)
			->setSetting('max_length', 255)
			->setTranslatable(TRUE)
			->setDisplayOptions('view', [
				'label' => 'hidden',
				'type' => 'text_default',
				'weight' => 0,
			])
			->setDisplayConfigurable('view', TRUE)
			->setDisplayOptions('form', [
				'type' => 'text_textfield',
				'weight' => 0,
				'required' => TRUE,
			])
			->setDisplayConfigurable('form', TRUE);

		$fields['created'] = BaseFieldDefinition::create('created')
			->setLabel(t('Created'))
			->setDescription('Fecha creación de la entidad.');

		$fields['changed'] = BaseFieldDefinition::create('changed')
			->setLabel(t('Changed'))
			->setDescription('Fecha actualización de la entidad');

		return $fields;
	}

}

?>